package server.service;

import org.apache.log4j.Logger;
import server.models.Bus;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

//This class defines api endpoints for CRUD operations implemented in BusService class
@Path("school")
@Produces(MediaType.APPLICATION_JSON)
public class Hello {

    public BusService busService;
    final static Logger logger = Logger.getLogger(Hello.class);

    public Hello(){ busService = new BusService(); }

    @GET
    @Path("bus")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bus> getAll() {
        List<Bus> bus_list = new ArrayList<>();
        busService.getAll(bus_list);
        for(int i=0; i<bus_list.size(); i++){
            System.out.println(bus_list.get(i).getDriver_name());
        }
        return bus_list;
    }

    @GET
    @Path("bus/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Bus getBus(@PathParam("param") int BusId){
        return busService.getBusById(BusId);
    }

    @POST
    @Path("bus/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bus addBus(Bus bus) {
        try{
            busService.addBus(bus);
        } catch (Exception e){
            logger.error(e);
        }
        return bus;
    }

    @DELETE
    @Path("bus/delete/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteBus(@PathParam("param") int BusId){
        return busService.deleteBus(BusId);
    }

    @PUT
    @Path("bus/update/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Bus updateBus(Bus bus, @PathParam("param") int BusId){
        try{
            busService.updateBus(bus, BusId);
        } catch(Exception e){
            logger.error(e);
        }
        return bus;
    }
}

